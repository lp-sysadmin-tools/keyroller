#!/usr/bin/env python

from setuptools import setup, find_packages

import contextlib
import io
import os
import shutil
import sys

src_dir = os.path.dirname(sys.argv[0])
script = os.path.join(src_dir, 'keyroller.py')

def version():
    bindings = {}
    with io.open(script) as infh:
        exec(infh.read(), bindings)
    return bindings['version']

class VirtualPackage:

    """Move the script in it's own module.

    The script is usable without installer but if it's packaged, it is converted to a
    proper module."""

    def __init__(self, src_dir):
        self.package_dir = os.path.join(src_dir, 'keyroller')
        os.mkdir(self.package_dir)
        shutil.copyfile(script, os.path.join(self.package_dir, '__init__.py'))

    def close(self):
        shutil.rmtree(self.package_dir)

with contextlib.closing(VirtualPackage(src_dir)):
    setup(name='keyroller',
          version=version(),
          description='Bind9 key renewal tool',
          long_description = "Renew DNSSec keys and delete old ones.",
          author='Laurent Pelecq',
          author_email='lpelecq+keyroller@circoise.eu',
          entry_points = {
              "console_scripts": ['keyroller = keyroller:main']
          },
          packages = find_packages(),
          test_suite="tests",
          license='GNU General Public License version 3'
          )
