# Copyright (C) 2019 Laurent Pelecq <lpelecq+keyroller@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import base64
import collections
import io
import keyroller
import os
import random
import tempfile
import time

__all__ = [ 'KeyGenerator' ]

STATES = [ 'created', 'publish', 'activate', 'inactive', 'delete' ]

HEADER_FMT = "; This is a {kind}-signing key, keyid {id}, for {domain}\n"
RECORD_FMT = "{domain} IN DNSKEY {kind} {record}\n"

KEY_SAMPLE = '0besPzJAjsCiaxdqOKrxRa8LP9phxMsGAYrFLh78V/AoCgnz0qmoCvf8'
SIGN_SAMPLE = 'llCjmDVth7X7hphICfiPDE7CWmFKeA'

RECORDS = [ '3 13 WXx8ZqoSRpjwu8zCQEVWxnqsrtcpyjIKZb6BTuOxvRSw6e0fjkaaFU7r fMNTtD0YY20T5Sc5RoIeIO+Fw3UQbA==',
            '3 13 C/4Qo1R2fYd0m5irDm0zbJ8P7M0n97AAkKDwpOo3smuJ/mYP/R9lFB/E B3Z5G1dmMsr4/E3CzoWqZfXSvpVlAQ==',
            '3 13 R4ShjuVFOZkUHzMfg7S0ZoJvKzqyHK/wNIG1ei5fHT+swnmW8yOIapN9 kvjI6h1k+rQkGAeExRwsKkfADMdIvQ==',
            '3 13 Cwull3Kyp7gLHX7DzInOYMuLhMOsdms8SGKqWCQQTdDhvEG2nSh5VVRC vQ8UgZE3FPgqpUX03JWmFfcWe0EIUA==',
            '3 13 RH33rxStZBaX7YXzFZe4pv3qhHjSfIgXGuwv8Yj1qQnL/hdWyMW7aGrd /efDzwjmsFhMf1ILBEvX472SZ8OTZg==' ]

KeyDefinition = collections.namedtuple('KeyDefinition',
                                       ('output', 'name', 'id',
                                        'creation_time', 'inactive_time', 'deletion_time'))

def format_timestamp(ts):
    return time.strftime("%Y%m%d%H%M%S (%a %b %d %X %Y)", time.localtime(ts))

def key_file(keys_dir, name):
    return os.path.join(keys_dir, '{}.key'.format(name))

class KeyGenerator:

    DAY_IN_SECONDS = 24 * 3600

    PERIODS = {
        'ksk': { 'active': 365 * DAY_IN_SECONDS, 'refresh': 30 * DAY_IN_SECONDS, 'delete': 40 * DAY_IN_SECONDS },
        'zsk': { 'active': 30 * DAY_IN_SECONDS, 'refresh': 5 * DAY_IN_SECONDS, 'delete': 10 * DAY_IN_SECONDS }
    }

    KEY_KINDS = { 'ksk': ('key', 257), 'zsk': ('zone', 256) }

    def __init__(self, domain, ref_time=None):
        self.domain = domain if domain.endswith('.') else '{}.'.format(domain)
        self.ref_time = ref_time or time.time()

    def period(self, kind, state):
        return self.PERIODS[kind][state]

    def periods(self, kind):
        periods = self.PERIODS[kind]
        return (periods['active'], periods['refresh'], periods['delete'])

    def __generate(self, stream_factory, stream_value, age, kind):
        """Create a fake key file content as generated by dnssec-keygen.

        If the current time is in the refresh period or after, the key has a inactive and delete
        timestamp.
        """
        key_id = random.randrange(10000, 100000)
        key_name = 'K{domain}+013+{key_id}'.format(domain=self.domain, key_id=key_id)
        ref_time = self.ref_time
        active_period, refresh_period, delete_period = self.periods(kind)
        creation_time = ref_time - age
        inactive_time = creation_time + active_period
        deletion_time = inactive_time + delete_period
        kind_name, kind_id = self.KEY_KINDS[kind]
        with stream_factory(key_name) as outfh:
            outfh.write(HEADER_FMT.format(kind=kind_name, id=key_id, domain=self.domain))
            for state in STATES[0:3]:
                outfh.write('; {}: {}\n'.format(state.capitalize(), format_timestamp(creation_time)))
            if inactive_time <= ref_time or inactive_time - ref_time < refresh_period:
                for state, state_time in [ (state[3], inactive_time), (state[4], deletion_time) ]:
                    outfh.write('; {}: {} ({})\n'.format(state, format_timestamp(state_time)))
            record = random.choice(RECORDS)
            outfh.write(RECORD_FMT.format(domain=self.domain, kind=kind_id, record=record))
            output = stream_value(outfh)
            return KeyDefinition(output, key_name, key_id, int(creation_time), int(inactive_time), int(deletion_time))

    def __generate_content(self, age, kind):
        return self.__generate(lambda name: io.StringIO(), lambda stream: stream.getvalue(), age, kind)

    def generate_ksk(self, age=0):
        return self.__generate_content(age, 'ksk')

    def generate_zsk(self, outfh=None, age=0):
        return self.__generate_content(age, 'zsk')

    def __save_key(self, keys_dir, age, kind):
        return self.__generate(lambda name: io.open(key_file(keys_dir, name), 'w'),
                               lambda stream: stream.name,
                               age, kind)

    def save_ksk(self, keys_dir, age=0):
        return self.__save_key(keys_dir, age, 'ksk')

    def save_zsk(self, keys_dir, age=0):
        return self.__save_key(keys_dir, age, 'zsk')

    def parse_key(self, content, name, keys_dir='/etc/bind/keys'):
        """Parse an in-memory key."""
        key = keyroller.DnsKey(self.ref_time)
        filename = key_file(keys_dir, name)
        with io.StringIO(content) as infh:
            key.parse(infh, filename=filename, name=name)
        return key

    def parse_key_definition(self, kdef, keys_dir='/etc/bind/keys'):
        """Parse a definition returned by generate_ksk or generate_zsk."""
        return self.parse_key(kdef.output, kdef.name, keys_dir=keys_dir)

    def load_key(self, filename):
        """Load a key"""
        key = keyroller.DnsKey(self.ref_time)
        key.load(filename)
        return key
