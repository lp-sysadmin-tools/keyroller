# Copyright (C) 2019 Laurent Pelecq <lpelecq+keyroller@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

__all__ = [ 'assert_has_option' ]

def assert_has_option(option, value, cmdline):
    """Check if a command line has an option."""
    try:
        index = cmdline.index(option)
        if index >= 0 and index + 1 < len(cmdline) and cmdline[index + 1] == value:
            return
    except ValueError:
        pass # raise by list from call
    raise AssertionError('Option {} with value {} not found in {}'.format(option, value, cmdline))
