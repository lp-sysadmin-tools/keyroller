#! /usr/bin/env python3
# Copyright (C) 2019 Laurent Pelecq <lpelecq+keyroller@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import collections
import io
import logging
import os
import subprocess
import sys
import tempfile
import time
import unittest

from unittest.mock import patch

import keyroller

def make_script(filename, lines):
    """Create an executable script."""
    with io.open(filename, 'w') as outfh:
        for line in lines:
            outfh.write('{}\n'.format(line))

class TestCollections(unittest.TestCase):

    """Test functions that create collections"""

    def test_hashBy(self):
        """Test function hash_by."""
        EXPECTED_KEYS = [ 'abc', 'def' ]
        Item = collections.namedtuple('Item', ('key', 'value'))
        item1 = Item('abc', 1)
        item2 = Item('def', 2)
        result = keyroller.hash_by('key', [ item1, item2 ])
        self.assertEqual(EXPECTED_KEYS, sorted([ key for key in result.keys() ]))
        for key in EXPECTED_KEYS:
            self.assertEqual(key, result[key].key)
        self.assertEqual(1, result['abc'].value)
        self.assertEqual(2, result['def'].value)

class TestFormat(unittest.TestCase):

    """Test format functions"""

    def test_formatDelay(self):
        """Test function format_delay."""
        self.assertEqual('5 seconds', keyroller.format_delay(5))
        self.assertEqual('2 minutes', keyroller.format_delay(127))
        self.assertEqual('3 hours', keyroller.format_delay(188 * 60 + 40))

    def test_formatDnskeyTime(self):
        """Test function format_dnssec_time."""
        self.assertEqual('20190420161743', keyroller.format_dnssec_time(time.gmtime(1555777063)))

class TestSafeCall(unittest.TestCase):

    """Test class SafeCall"""

    def test_function(self):
        """Safe call on function."""
        def func(hmap, key, value):
            hmap[key] = value
        for simulate, expected_result in [ (False, { "x": 1 }), (True, {}) ]:
            exec_call = keyroller.SafeCall(simulate)
            exec_func = exec_call(func)
            hmap = {}
            exec_func(hmap, "x", 1)
            self.assertEqual(expected_result, hmap)

    def test_method(self):
        """Safe call on function."""
        class Target:
            def __init__(self):
                self.hmap = {}
            def func(self, key, value):
                self.hmap[key] = value
        for simulate, expected_result in [ (False, { "x": 1 }), (True, {}) ]:
            exec_call = keyroller.SafeCall(simulate)
            target = Target()
            exec_func = exec_call(Target.func)
            exec_func(target, "x", 1)
            self.assertEqual(expected_result, target.hmap)

    def test_execute_subprocess(self):
        """Execute a subprocess.run"""
        safecall = keyroller.SafeCall(False)
        def subprocess_result(args):
            return subprocess.CompletedProcess(args[0], 0)
        run = safecall(subprocess.run, subprocess_result)
        with tempfile.TemporaryDirectory() as tmp_dir:
            script = os.path.join(tmp_dir, 'touch.py')
            res_file = os.path.join(tmp_dir, 'res.txt')
            make_script(script, [ 'import io',
                                  'with io.open("{}", "w") as fh:'.format(res_file),
                                  '\tpass' ])
            self.assertFalse(os.path.exists(res_file))
            cmdline = [ sys.executable, script ]
            run = safecall(subprocess.run, subprocess_result)
            ret = run(cmdline)
            self.assertTrue(isinstance(ret, subprocess.CompletedProcess))
            self.assertEqual(cmdline, ret.args)
            self.assertEqual(0, ret.returncode)
            self.assertTrue(os.path.exists(res_file))

    def test_simulate_subprocess(self):
        """Simulate a subprocess.run"""
        safecall = keyroller.SafeCall(True)
        def subprocess_result(args):
            return subprocess.CompletedProcess(args[0], 0)
        run = safecall(subprocess.run, subprocess_result)
        cmdline = ['/not/a/real/command', 'arg']
        ret = run(cmdline)
        self.assertTrue(isinstance(ret, subprocess.CompletedProcess))
        self.assertEqual(cmdline, ret.args)
        self.assertEqual(0, ret.returncode)

    @patch('logging.debug')
    def test_os_chown(self, mock_log_debug):
        """Simulate a os.chown"""
        safecall = keyroller.SafeCall(True)
        run = safecall(os.chown)
        ret = run("/not/a/real/file", 0, 0)
        self.assertIsNone(ret)
        mock_log_debug.assert_called_with('calling: chown(/not/a/real/file, 0, 0)')

    def test_os_getpid(self):
        """Simulate a os.getpid"""
        def getpid_result(*args):
            return 15
        for simulate, expected_result in [ (True, 15), (False, os.getpid()) ]:
            safecall = keyroller.SafeCall(simulate)
            run = safecall(os.getpid, getpid_result)
            ret = run()
            self.assertEqual(expected_result, ret)


class TestOtherFunctions(unittest.TestCase):

    """Test other functions"""

    def test_initializeLogger(self):
        """Test logging combinations"""
        # stdout
        for is_verbose, is_debug, expected_level in [ (False, False, logging.WARNING),
                                                      (True, False, logging.INFO),
                                                      (False, True, logging.DEBUG) ]:
            logger = logging.getLogger("test_stdout{}".format(expected_level))
            with patch("logging.getLogger", return_value=logger):
                self.assertFalse(logger.hasHandlers())
                keyroller.initialize_logger(is_verbose, is_debug, False)
                self.assertEqual(expected_level, logger.level, "expecting logger level {}".format(expected_level))
                self.assertTrue(logger.hasHandlers())
                self.assertTrue(isinstance(logger.handlers[0], logging.StreamHandler))

        logger = logging.getLogger("test_syslog")
        with patch("logging.getLogger", return_value=logger):
            self.assertFalse(logger.hasHandlers())
            keyroller.initialize_logger(False, False, True)
            self.assertTrue(logger.hasHandlers())
            self.assertTrue(isinstance(logger.handlers[0], logging.handlers.SysLogHandler))


if __name__ == "__main__":
    unittest.main()
