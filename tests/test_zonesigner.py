#! /usr/bin/env python3
# Copyright (C) 2019 Laurent Pelecq <lpelecq+keyroller@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import difflib
import io
import os
import shutil
import tempfile
import time
import unittest

import keyroller

from unittest.mock import patch
from tests.helpers.keys import KeyGenerator
from tests.helpers.asserts import *

DOMAIN = 'example.net'

ZONE_HEADER = """; Domain example.net
;
$TTL    6h
@ IN SOA ns.example.net. root.example.net. (
                     2019112300         ; Serial
                             4h         ; Refresh
                             1h         ; Retry
                             4w         ; Expire
                             1h )       ; Negative Cache TTL
"""

ZONE_RECORDS = """; DNS
@ IN NS         ns1.example.net.
@ IN NS         ns2.example.net.
; SMTP
@ IN MX         10 mail.example.net.
mail            IN A 128.66.0.181
mail            IN AAAA 2001:DB8::47:216:3eff:fec7:2b80
; Hosts
sun             IN A 128.66.0.181
sun             IN AAAA 2001:DB8::47:216:3eff:fec7:2b80
; Aliases
www             IN CNAME sun.example.net.
smtp            IN CNAME mail.example.net.
"""

ZONE_INITIAL = ZONE_HEADER + ZONE_RECORDS

ZONE_KEYS = """;
; DNS keys
$INCLUDE {dir}/example.net.keys
"""

SUB_DOMAIN = """;
; Subdomain sun.example.net
$ORIGIN sub.example.net.
www             IN CNAME sun.example.net.
"""

ZONE_WITH_KEYS = ZONE_INITIAL + ZONE_KEYS
ZONE_WITH_ORIGIN = ZONE_INITIAL + SUB_DOMAIN
ZONE_WITH_KEYS_AND_ORIGIN = ZONE_WITH_KEYS + SUB_DOMAIN

def create_file(filename, content):
    with io.open(filename, 'w') as outfh:
        outfh.write(content)

def compact_diff(old_lines, new_lines):
    """Return only removed, different or added lines."""
    diffs = []
    for delta in difflib.Differ().compare(old_lines, new_lines):
        if delta.startswith('- ') or delta.startswith('+ '):
            diffs.append((delta[0], delta[2:]))
        elif delta.startswith('? '):
            last_diff = diffs.pop()
            if last_diff[0] == '-':
                diffs.append(('?', last_diff[1]))
            elif last_diff[0] == '+':
                previous_diff = diffs.pop()
                if not previous_diff[0] == '?':
                    raise Exception('internal error')
                diffs.append(('?', previous_diff[1], last_diff[1]))
    return diffs

def read_file(filename):
    """Read file lines."""
    with io.open(filename) as infh:
        return infh.readlines()

def diff_files(left_file, right_file):
    return compact_diff(read_file(left_file), read_file(right_file))

def file_inode(filename):
    return os.stat(filename).st_ino

class TestZoneSigner(unittest.TestCase):

    """Test class ZoneSigner"""

    def setUp(self):
        self.ref_time_tuple = time.strptime("2019/10/24 22:10:45", "%Y/%m/%d %H:%M:%S")
        self.ref_time = time.mktime(self.ref_time_tuple)
        self.working_dir = tempfile.mkdtemp(prefix='tmp_keyroller')
        self.keys_dir = os.path.join(self.working_dir, 'keys')
        self.zone_dir = os.path.join(self.working_dir, 'zones')
        for folder in [ self.keys_dir, self.zone_dir ]:
            os.mkdir(folder)
        self.zone_file = os.path.join(self.zone_dir, DOMAIN)
        self.keys_file = '{}.keys'.format(self.zone_file)
        # Zone content
        self.zone_with_keys = ZONE_WITH_KEYS.format(dir=self.zone_dir)
        # Keys
        kg = KeyGenerator(DOMAIN, ref_time=self.ref_time)
        ksk_age = kg.period('ksk', 'active')/2
        ksk_def = kg.save_ksk(self.keys_dir, ksk_age)
        self.ksk = kg.load_key(ksk_def.output)
        zsk1_age = kg.period('zsk', 'active')-kg.period('zsk', 'refresh')/2
        zsk1_def = kg.save_ksk(self.keys_dir, zsk1_age)
        self.zsk1 = kg.load_key(zsk1_def.output)
        zsk2_age = kg.period('zsk', 'active')/2
        zsk2_def = kg.save_ksk(self.keys_dir, zsk2_age)
        self.zsk2 = kg.load_key(zsk2_def.output)

    def tearDown(self):
        shutil.rmtree(self.working_dir)

    def sign(self, zsk_list, zone_content=None):
        """Sign the zone and return the mock for run."""
        if zone_content:
            create_file(self.zone_file, zone_content)
        signer = keyroller.ZoneSigner(DOMAIN, self.zone_dir, 1, [ self.ksk ], zsk_list, now=self.ref_time_tuple)
        with patch.object(signer.system, 'run') as mock_run:
            has_signed = signer.sign('raw')
            if mock_run.call_count > 0:
                # create fake signed file
                create_file(signer.signed_file, '')
            return mock_run, has_signed

    @patch('logging.warning')
    def test_signingFirstTime(self, mock_log_warn):
        """Sign zone first time."""
        self.assertFalse(os.path.isfile(self.keys_file))
        mock_run, has_signed = self.sign([ self.zsk1 ], self.zone_with_keys)
        mock_run.assert_called_once()
        self.assertTrue(has_signed)
        assert_has_option('-O', 'raw', mock_run.call_args.args[0])
        self.assertTrue(os.path.isfile(self.keys_file))
        # Check no double signature
        keys_ino = file_inode(self.keys_file)
        zone_ino = file_inode(self.zone_file)
        mock_run, has_signed = self.sign([ self.zsk1 ])
        mock_run.assert_not_called()
        self.assertFalse(has_signed)
        self.assertEqual(keys_ino, file_inode(self.keys_file))
        self.assertEqual(zone_ino, file_inode(self.zone_file))
        mock_log_warn.assert_not_called()

    @patch('logging.warning')
    def test_signingOnKeyChange(self, mock_log_warn):
        """Sign zone when keys change."""
        mock_run, has_signed = self.sign([ self.zsk1 ], self.zone_with_keys)
        mock_run.assert_called_once()
        self.assertTrue(has_signed)
        includes1 = read_file(self.keys_file)
        # Sign on key change
        mock_run, has_signed = self.sign([ self.zsk2 ])
        mock_run.assert_called_once()
        self.assertTrue(has_signed)
        includes2 = read_file(self.keys_file)
        expected_diff = [ ('?',
                           '$INCLUDE {}\n'.format(self.zsk1.filename),
                           '$INCLUDE {}\n'.format(self.zsk2.filename)) ]
        diff = compact_diff(includes1, includes2)
        self.assertEqual(expected_diff, diff)

    @patch('logging.warning')
    def test_warnNoKeyIncluded(self, mock_log_warn):
        """Check if keys are included."""
        mock_run, _ = self.sign([ self.zsk1 ], ZONE_INITIAL)
        mock_run.assert_called_once()
        mock_log_warn.assert_called_once()

if __name__ == "__main__":
    unittest.main()
