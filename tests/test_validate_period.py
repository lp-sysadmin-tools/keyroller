#! /usr/bin/env python3
# Copyright (C) 2019 Laurent Pelecq <lpelecq+keyroller@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import random
import unittest

import keyroller

class TestValidatePeriod(unittest.TestCase):

    """Test class ValidatePeriod"""

    def test_noDecode(self):
        """Test validation without decoding."""
        for unit in [ 'mi', 'minute', 'minutes', 'h', 'hour', 'hours', 'd', 'day', 'days',
                      'w', 'week', 'weeks', 'mo', 'month', 'months', 'y', 'year', 'years' ]:
            period = '{}{}'.format(random.randint(1, 20), unit)
            self.assertEqual(period, keyroller.ValidatePeriod()(period), msg="wrong value for period {}".format(period))
        for unit in [ 'k', 'kilo' ]:
            with self.assertRaises(ValueError):
                keyroller.ValidatePeriod()("4{}".format(unit))

    def test_decode(self):
        """Test validation and decoding."""
        self.assertEqual(12 * 60, keyroller.ValidatePeriod.decode("12mi"))
        self.assertEqual(3 * 3600, keyroller.ValidatePeriod.decode("3h"))
        ONE_DAY_SECONDS = 24 * 3600
        self.assertEqual(4 * ONE_DAY_SECONDS, keyroller.ValidatePeriod.decode("4d"))
        self.assertEqual(1 * 7 * ONE_DAY_SECONDS, keyroller.ValidatePeriod.decode("1w"))
        self.assertEqual(3 * 30 * ONE_DAY_SECONDS, keyroller.ValidatePeriod.decode("3mo"))
        self.assertEqual(2 * 365 * ONE_DAY_SECONDS, keyroller.ValidatePeriod.decode("2y"))

if __name__ == "__main__":
    unittest.main()
