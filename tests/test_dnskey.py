#! /usr/bin/env python3
# Copyright (C) 2019 Laurent Pelecq <lpelecq+keyroller@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import io
import random
import time
import unittest

import keyroller

from tests.helpers.keys import KeyGenerator

class TestDnsKey(unittest.TestCase):

    """Test class DnsKey"""

    def test_parseKey(self):
        """Parse key."""
        now = time.time()
        kg = KeyGenerator('example.net.', now)
        age = kg.period('zsk', 'active')/2
        kdef = kg.generate_zsk(age=age)
        key = kg.parse_key_definition(kdef)
        self.assertEqual(str(kdef.id), key.id)
        self.assertEqual(kdef.creation_time, int(key.get_unix_time('created')))
        self.assertEqual(kdef.creation_time, int(key.get_unix_time('publish')))
        self.assertEqual(kdef.creation_time, int(key.get_unix_time('activate')))
        self.assertEqual(kdef.creation_time, int(key.get_activation_time()))
        self.assertIsNone(key.get_unix_time('inactive'))
        self.assertIsNone(key.get_unix_time('delete'))

    def test_compareKeys(self):
        """Compare two keys."""
        now = time.time()
        kg = KeyGenerator('example.net.', now)
        age1 = kg.period('zsk', 'active')/2
        kdef1 = kg.generate_zsk(age=age1)
        kdef2 = kg.generate_zsk(age=age1-kg.DAY_IN_SECONDS)
        # Two keys with different timestamps
        key1 = kg.parse_key_definition(kdef1)
        key2 = kg.parse_key_definition(kdef2)
        self.assertTrue(key1 < key2)
        self.assertNotEqual(key1, key2)
        self.assertFalse(key1 > key2)
        # Two keys with same timestamps except inactivation time
        key1 = kg.parse_key_definition(kdef1)
        key2 = kg.parse_key_definition(kdef1)
        self.assertEqual(key1, key2)
        self.assertFalse(key1 < key2)
        self.assertFalse(key1 > key2)
        now = time.time()
        key2.set_inactivation_time(now)
        self.assertNotEqual(key1, key2)
        self.assertTrue(key1 < key2)
        self.assertFalse(key1 > key2)

if __name__ == "__main__":
    unittest.main()
