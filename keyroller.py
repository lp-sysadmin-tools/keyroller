#! /usr/bin/env python3

# Copyright (C) 2015-2020 Laurent Pelecq <lpelecq+keyroller@circoise.eu>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.

import argparse
import grp
import io
import locale
import os
import random
import re
import stat
import subprocess
import sys
import time

from collections import namedtuple
from functools import total_ordering

### Global

version = '2.4'

### Functions

def hash_by(attr_name, items):
    """Convert a list of objects to a dictionary using ATTR_NAME as key."""
    result = {}
    for item in items:
        result[getattr(item, attr_name)] = item
    return result

### Types

DigestType = namedtuple('DigestType', [ 'code', 'name', 'deprecated' ])

DIGEST_TYPES = hash_by('code',
    [ DigestType(1, 'SHA-1', True),
      DigestType(2, 'SHA-256', False),
      DigestType(3, 'GOST R 34.11-94', False),
      DigestType(4, 'SHA-384', False) ])

Algorithm = namedtuple('Algorithm', [ 'code', 'name', 'description', 'deprecated', 'zone_signing' ])

ALGORITHMS = hash_by('code',
    [ Algorithm( 1, "RSAMD5",		"RSA/MD5",				True, False),
      Algorithm( 2, "DH",		"Diffie-Hellman",			True, False),
      Algorithm( 3, "DSA",		"DSA/SHA1",				True, True),
      Algorithm( 5, "RSASHA1",		"RSA/SHA-1",				True, True),
      Algorithm( 6, "DSA-NSEC3-SHA1",	"DSA-NSEC3-SHA1",			True, True),
      Algorithm( 7, "RSASHA1-NSEC3-SHA1", "RSASHA1-NSEC3-SHA1", 		True, True),
      Algorithm( 8, "RSASHA256",	"RSA/SHA-256",				False, True),
      Algorithm(10, "RSASHA512",	"RSA/SHA-512",				False, True),
      Algorithm(12, "ECC-GOST",		"GOST R 34.10-2001",			False, True),
      Algorithm(13, "ECDSAP256SHA256",	"ECDSA Curve P-256 with SHA-256",	False, True),
      Algorithm(14, "ECDSAP384SHA384",	"ECDSA Curve P-384 with SHA-384",	False, True) ])

default_algorithm = ALGORITHMS[13].name

key_choices = ['KSK', 'ZSK']

nsec_choices = [1, 3]

default_delete_period = '1mo'

default_refresh_period = '5d'

default_validity_period = '1mo'

### Parsing command line

class ValidatePeriod:

    """Decode and validate periods expressed as a number followed by a unit.

    Units are mi(nute), h(our), d(ay), w(eek), mo(nth), y(ear)"""

    value_re = re.compile(r'^(?P<num>\d+)(?P<unit>[a-z]+)?$')

    MINUTE_IN_SECONDS = 60
    HOUR_IN_SECONDS = 60 * MINUTE_IN_SECONDS
    DAY_IN_SECONDS = 24 * HOUR_IN_SECONDS
    WEEK_IN_SECONDS = 7 * DAY_IN_SECONDS
    MONTH_IN_SECONDS = 30 * DAY_IN_SECONDS
    YEAR_IN_SECONDS = 365 * DAY_IN_SECONDS

    units = {
        'years': YEAR_IN_SECONDS,
        'months': MONTH_IN_SECONDS,
        'weeks': WEEK_IN_SECONDS,
        'days': DAY_IN_SECONDS,
        'hours': HOUR_IN_SECONDS,
        'minutes': MINUTE_IN_SECONDS
    }

    aliases = { 'y': 'years', 'year': 'years', 'mo': 'months', 'month': 'months',
                'w': 'weeks', 'week': 'weeks', 'd': 'days', 'day': 'days',
                'h': 'hours', 'hour': 'hours', 'mi': 'minutes', 'minute': 'minutes' }

    def __init__(self, decode=False, exception=ValueError):
        self.decode = decode
        self.exception = ValueError

    def __call__(self, value):
        m = self.value_re.match(value)
        if not m:
            raise argparse.ArgumentTypeError("{}: invalid period".format(value))
        unit = m.group('unit')
        if unit in self.aliases:
            unit = self.aliases[unit]
        if not unit in self.units:
            raise self.exception("{}: invalid time unit".format(unit))
        if self.decode:
            num = int(m.group('num'))
            return num * self.units[unit]
        return value

    @staticmethod
    def decode(value):
        return ValidatePeriod(True)(value)

help = """Manage DNSSec keys rollover.

Rollover takes place when option --key-type is specified. Otherwise, the current keys are listed.

Algorithms are: {}

Periods are in seconds except if followed by a unit 'y' for year, 'mo' for month, 'w' for week,
'd' for day, 'h' for hour or 'mi' for minutes.""".format(", ".join([ a.name for a in ALGORITHMS.values() ]))

def parse_argv(args=None):
    valid_period = ValidatePeriod(exception=argparse.ArgumentTypeError)
    argparser = argparse.ArgumentParser(description=help, formatter_class=argparse.RawDescriptionHelpFormatter)

    # Logging

    argparser.add_argument("--debug",
                           dest="debug", action="store_true", default=False,
                           help="Generate debug output.")

    argparser.add_argument("--syslog",
                           dest="use_syslog", action="store_true", default=False,
                           help="Use syslog for logging.")

    argparser.add_argument("-v", "--verbose",
                           dest="verbose", action="store_true", default=False,
                           help="Generate verbose output.")

    # Running mode

    argparser.add_argument("-n", "--dry-run", "--simulate",
                           dest="simulate", action="store_true", default=False,
                           help="Do not make any change.")

    # Key generation

    argparser.add_argument("-K", "--keys-dir",
                           dest="keys_dir", action="store", default="/etc/bind/keys",
                           metavar="DIR", help="directory of keys.")

    argparser.add_argument("-a", "--algorithm",
                           dest="algorithm", action="store", default=default_algorithm,
                           help="Encryption algorithm (default {}).".format(default_algorithm))

    argparser.add_argument("-b", "--key-size",
                           dest="key_size", action="store", type=int, default=2048,
                           metavar='SIZE', help="Key size.")

    argparser.add_argument("-D", "--digest-type",
                           dest="digest_type", action="store", default=DIGEST_TYPES[2].name,
                           metavar='NAME', help="Digest type for DS records.")

    argparser.add_argument("-g", "--group",
                           dest="group", action="store", default='-1',
                           metavar='NAME_OR_ID', help="System group that must have read access.")

    argparser.add_argument("-N", "--next-secure",
                           dest="nsec", action="store", type=int, choices=nsec_choices, default=nsec_choices[0],
                           metavar='N', help="Next secure style (default {}).".format(nsec_choices[0]))

    # Rollover

    argparser.add_argument("-Z", "--zones-dir",
                           dest="zones_dir", action="store", default="/etc/bind/zones",
                           metavar="DIR", help="directory of zone files.")

    argparser.add_argument("-T", "--key-type",
                           dest="key_type", action="store", choices=key_choices, default=None,
                           metavar='TYPE', help="Type of key ({}).".format(", ".join(key_choices)))

    argparser.add_argument("-d", "--delete",
                           dest="delete_period", action="store", type=valid_period, default=default_delete_period,
                           metavar='PERIOD',
                           help="Delete period, i.e. delay before deletion of the keys (default: {}).".format(default_delete_period))

    argparser.add_argument("-r", "--refresh",
                           dest="refresh_period", action="store", type=valid_period, default=default_refresh_period,
                           metavar='PERIOD',
                           help="Refresh period, i.e. delay before expiration to renew the keys (default: {}).".format(default_refresh_period))

    argparser.add_argument("-i", "--validity",
                           dest="validity_period", action="store", type=valid_period, default=default_validity_period,
                           metavar='PERIOD',
                           help="Validity period, i.e. interval of time where a signature is valid (default: {}).".format(default_validity_period))

    argparser.add_argument("-V", "--view",
                           dest="view", action="store", default=None,
                           help="Bind view")

    argparser.add_argument("-s", "--sign", choices=('raw', 'text', 'map', 'full'),
                           dest="sign_mode", action="store", default=None,
                           help="Sign zone if bind is not in inline mode.")

    argparser.add_argument("domains", nargs='+', action="store", metavar="DOMAIN")

    return argparser.parse_args(args=args)

############################################################
# Logger

import logging
import logging.handlers

def initialize_logger(is_verbose, is_debug, use_syslog):
    """Initialize a logger."""
    logger = logging.getLogger()
    if not use_syslog:
        handler = logging.StreamHandler()
        handler.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
    else:
        handler = logging.handlers.SysLogHandler('/dev/log', logging.handlers.SysLogHandler.LOG_DAEMON)
        handler.setFormatter(logging.Formatter('%(name)s: %(levelname)s: %(message)s'))
    logger.addHandler(handler)

    if is_debug:
        level = logging.DEBUG
    elif is_verbose:
        level = logging.INFO
    else:
        level = logging.WARNING
    logger.setLevel(level)

############################################################
# Running mode

class SafeCall:

    """Safe call.

    Make a call only if not in simulation mode.
    """

    def __init__(self, simulate=False):
        self.simulate = simulate

    def __call__(self, function, map_result=None):
        """Execute or simulate function call.

        MAP_RESULT is a function that takes the arguments as a list and build a result in simulation."""
        if self.simulate:
            def inner(*args, **kw):
                fname = function.__name__
                if args and hasattr(args[0], fname):
                    args = list(args)
                    obj = args.pop(0)
                    fname = '{}.{}'.format(obj.__class__.__name__, fname)
                args_strings = [ str(x) for x in args ]
                kw_strings = [ '{}={}'.format(k, kw[k]) for k in kw ]
                logging.debug('calling: {}({})'.format(fname, ', '.join(args_strings + kw_strings)))
                if map_result:
                    return map_result(args)
            return inner
        return function

class SafeSystem:

    """Collections of methods that aren't called in simulation mode."""

    def __init__(self, simulate):
        self.simulate = simulate
        safecall = SafeCall(simulate)
        def subprocess_result(args):
            return subprocess.CompletedProcess(args[0], 0)
        try:
            self.run = safecall(subprocess.run, subprocess_result)
        except AttributeError:
            self.run = safecall(subprocess.call, subprocess_result)
        self.chown = safecall(os.chown)
        self.chmod = safecall(os.chmod)
        self.unlink = safecall(os.unlink)
        self.rename = safecall(os.rename)
        self.symlink = safecall(os.symlink)

    def is_simulate(self):
        return self.simulate

############################################################
# DNS

try:
    import dns.rdatatype
    import dns.message
    import dns.query
    import dns.resolver
    import dns.rcode

    DSRecord = namedtuple('DSRecord', ('key_tag', 'algorithm', 'digest_type', 'digest'))

    class Resolver:

        """DNS resolver."""

        RETRY = 4

        TIMEOUT = 0.4

        def __init__(self, domain):
            self.domain = domain
            self.resolver = dns.resolver.get_default_resolver().nameservers[0]

        def __query(self, name, data_type):
            """Query the DNS."""
            message = dns.message.make_query(name, data_type, use_edns=0, payload=4096, want_dnssec=True)
            done = False
            retry = self.RETRY
            while retry > 0:
                try:
                    response = dns.query.udp(message, self.resolver, timeout=self.TIMEOUT)
                    if response.rcode() != dns.rcode.NOERROR:
                        raise Exception(dns.rcode.to_text(response.rcode()))
                    return response.answer
                except dns.exception.Timeout:
                    pass
                retry -= 1
            raise Exception("{}: can't contact name server for domain {}".format(self.resolver, self.domain))

        def __get_first(self, name, data_type, member):
            """Get the first answer of a given type."""
            answers = self.__query(name, data_type)
            for answer in answers:
                for item in answer:
                    if item.rdtype == data_type:
                        result = getattr(item, member)
                        if hasattr(result, 'to_text'):
                            result = result.to_text()
                        return result
            return None

        def __get_address(self, name):
            """Get the first IPv4 or IPv6 address."""
            addr = self.__get_first(name, dns.rdatatype.A, 'address')
            if not addr:
                return self.__get_first(name, dns.rdatatype.AAAA, 'address')
            return addr

        def resolve_ns(self):
            """Get the Name Server first address."""
            ns = self.__get_first(self.domain, dns.rdatatype.NS, 'target')
            if ns:
                logging.info("{} name server is {}".format(self.domain, ns))
                address = self.__get_address(ns)
                if address:
                    logging.info("{} name server address is {}".format(self.domain, address))
                    self.resolver = address

        def get_expiration_time(self):
            """Get the minimum and maximum expiration time of RRSIG records."""
            answers = self.__query(self.domain, dns.rdatatype.RRSIG)
            min_expiration = max_expiration = None
            for answer in answers:
                for item in answer.items:
                    if item.type_covered == dns.rdatatype.DNSKEY:
                        if not min_expiration or item.expiration < min_expiration:
                            min_expiration = item.expiration
                        if not max_expiration or item.expiration > max_expiration:
                            max_expiration = item.expiration
            return (min_expiration, max_expiration)

        def get_ds_records(self):
            """Get the DS records in the parent zone."""
            answers = self.__query(self.domain, dns.rdatatype.DS)
            records = []
            for answer in answers:
                for item in answer.items:
                    if isinstance(item, dns.rdtypes.ANY.DS.DS):
                        records.append(DSRecord(item.key_tag, ALGORITHMS[item.algorithm], DIGEST_TYPES[item.digest_type], item.digest))
            return records

    resolver_available = True
except ImportError:
    resolver_available = False

############################################################
# Main

def format_delay(delay, plus_sign=''):
    """Format a duration in seconds with the closest unit (for tracing only)."""
    sign = plus_sign
    if delay < 0:
        sign = '-'
        delay = -delay
    units = [(1, 'seconds'), (60, 'minutes'), (60, 'hours'), (24, 'days')]
    unit = ''
    for value, name in units:
        if delay > value:
            unit = name
            delay /= value
        else:
            break
    return '{}{} {}'.format(sign, int(delay), unit)

def format_timestamp(timestamp, ref=None):
    """Convert a UNIX time to a string."""
    delay = ''
    if ref:
        delay = " ({})".format(format_delay(timestamp - ref, plus_sign='+'))
    return time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(timestamp)) + delay

def format_dnssec_time(timestamp):
    """Format a UNIX time for dnssec-* functions YYYYMMDDHHMMSS"""
    return time.strftime('%Y%m%d%H%M%S', timestamp)

def get_group_id(name):
    """Return the system group id."""
    try:
        return int(name)
    except ValueError:
        entry = grp.getgrnam(name)
        return entry.gr_gid

class SyntaxError(Exception):

    """Syntax error in a file."""

    def __init__(self, filename, lineno, msg):
        super().__init__(self, '{}:{}: {}'.format(filename, lineno, msg))

############################################################
# Key

@total_ordering
class DnsKey:

    """A cryptographic key description."""

    ZSK = 256
    KSK = 257

    KEY_KINDS = { ZSK: 'ZSK', KSK: 'KSK' }

    id_re = re.compile(r'; This is a (?:key|zone)-signing key, keyid (?P<id>\d+), for (?P<domain>.*)')
    timestamp_re = re.compile(r'; (?P<kind>\w+): (?P<time>\d{14}) \(.*\)')

    # States come from the fields written by dnssec-keygen in .key files.
    STATES = [ 'created', 'publish', 'activate', 'inactive', 'delete' ]

    def __init__(self, ref_time):
        self.ref_time = ref_time
        self.filename = None
        self.id = None
        self.domain = None
        self.timestamps = {}
        self.timeline = None
        self.kind = None
        self.protocol = None
        self.algorithm = None
        self.key = None
        self.display_name = None
        self.name = None

    def __eq__(self, other):
        """Compare ids and timestamps."""
        if self.id == other.id:
            for state in self.STATES:
                if self.get_unix_time(state) != other.get_unix_time(state):
                    return False
            return True
        return False

    def __lt__(self, other):
        """Compare keys by timestamps."""
        for state in self.STATES:
            sts = self.get_unix_time(state)
            ots = other.get_unix_time(state)
            if not sts:
                if ots:
                    return True
                else:
                    break # No timestamps, compare ids
            if sts < ots:
                return True
        return self.id < other.id

    def get_state(self):
        """Key state from 'created' to 'delete'."""
        current_state = None
        for state in self.STATES:
            if state in self.timestamps:
                timestamp = self.timestamps[state]
                if time.mktime(timestamp) < self.ref_time:
                    current_state = state
                else:
                    break
            else:
                break
        return current_state

    def get_time(self, name):
        """Time tuple corresponding to state NAME."""
        if name in self.timestamps:
            return self.timestamps[name]
        return None

    def get_unix_time(self, name):
        """Time corresponding to state NAME in seconds since the epoch."""
        time_tuple = self.get_time(name)
        if time_tuple:
            return time.mktime(time_tuple)
        return None

    def get_delay_overdue(self, name):
        """Number of seconds after the timestamp NAME has expired.

        If the timestamp hasn't expired yet, return 0."""
        timestamp = self.get_unix_time(name)
        if timestamp and timestamp < self.ref_time:
            return self.ref_time - timestamp
        return 0

    def get_activation_time(self):
        """Activation time."""
        return self.get_unix_time('activate')

    def get_inactivation_time(self):
        """Inactivation time."""
        return self.get_unix_time('inactive')

    def set_inactivation_time(self, inactivation_time):
        """Set inactivation time if not available.

        Time is given as seconds since the epoch."""
        if 'inactive' in self.timestamps:
            raise Exception("{}: inactivation time already set".format(self.display_name))
        self.timestamps['inactive'] = time.localtime(inactivation_time)

    def get_timeline(self):
        """List of date when the states change (for tracing only)."""
        if not self.timeline:
            s = io.StringIO()
            separator = ''
            for name in self.STATES:
                timestamp = self.get_unix_time(name)
                if timestamp:
                    s.write("{}{}={}".format(separator, name, format_timestamp(timestamp)))
                    separator = ' '
            self.timeline = s.getvalue()
        return self.timeline

    def load(self, filename):
        """Load key information from a .key file generated by dnssec-keygen."""
        with io.open(filename) as infh:
            path = os.path.abspath(filename)
            name = os.path.splitext(os.path.basename(filename))[0]
            self.parse(infh, filename=path, name=name)

    def parse(self, infh, filename=None, name=None):
        """Parse a key from a stream."""
        try:
            self.filename = filename or 'fd<{}>'.format(infh.fileno())
        except io.UnsupportedOperation:
            self.filename = 'stream'
        self.name = name
        lineno = 0
        for line in infh:
            lineno += 1
            if not self.id:
                m = self.id_re.match(line)
                if not m:
                    raise SyntaxError(filename, lineno, "invalid line")
                self.id = m.group('id')
                self.domain = m.group('domain')
            elif line.startswith(';'):
                m = self.timestamp_re.match(line)
                if not m:
                    raise SyntaxError(filename, lineno, "invalid timestamp")
                self.timestamps[m.group('kind').lower()] = time.strptime(m.group('time'), '%Y%m%d%H%M%S')
            elif not self.key:
                tokens = line.rstrip().split(' ', 6)
                try:
                    if len(tokens) != 7:
                        raise SyntaxError(filename, lineno, "invalid line")
                    elif tokens[1] != 'IN' or tokens[2] != 'DNSKEY':
                        raise SyntaxError(filename, lineno, "not a key record")
                    elif not int(tokens[3]) in [self.KSK, self.ZSK]:
                        raise SyntaxError(filename, lineno, "{}: invalid key type".format(tokens[3]))
                    elif int(tokens[4]) != 3:
                        raise SyntaxError(filename, lineno, "{}: unknown protocol".format(tokens[4]))
                    elif not int(tokens[5]) in ALGORITHMS:
                        raise SyntaxError(filename, lineno, "{}: unknown algorithm".format(tokens[5]))
                except ValueError:
                    raise SyntaxError(filename, lineno, "invalid DNSKEY record")
                self.kind = int(tokens[3])
                self.protocol = int(tokens[4])
                self.algorithm = ALGORITHMS[int(tokens[5])]
                self.key = tokens[6]
        self.display_name = '{} {}'.format(self.KEY_KINDS[self.kind], self.id)

    def reload(self):
        """Reload from disk"""
        self.id = None
        self.domain = None
        self.timestamps = {}
        self.timeline = None
        self.kind = None
        self.protocol = None
        self.algorithm = None
        self.key = None
        self.display_name = None
        self.name = None
        self.load(self.filename)

    def write_on(self, outfh):
        """Write the key description."""
        outfh.write("{} on {}\n".format(self.display_name, self.domain))
        outfh.write("state: {}\n".format(self.get_state()))
        outfh.write("algorithm: {}\n".format(self.algorithm.description))
        for state in self.STATES:
            if state in self.timestamps:
                timestamp = time.mktime(self.timestamps[state])
                outfh.write("- {:<10s} {}\n".format(state + ':', format_timestamp(timestamp, ref=self.ref_time)))

    @staticmethod
    def load_from_file(filename, ref_time):
        key = DnsKey(ref_time)
        key.load(filename)
        return key

############################################################
# Zone

class ZoneSigner:

    """Update the zone to include the keys and sign it."""

    def __init__(self, domain, zone_dir, nsec, ksk_list, zsk_list, now=None, simulate=False):
        self.domain = domain
        self.nsec = nsec
        if self.nsec == 3:
            raise NotImplementedError("no NSEC3 support")
        self.zone_dir = zone_dir
        self.system = SafeSystem(simulate)
        self.ksk = ksk_list[-1] # Only use the more recent KSK
        self.zsk_list = zsk_list
        self.now = now or time.localtime()
        self.key_dir = os.path.dirname(self.ksk.filename)
        name = domain[0:-1] if domain.endswith('.') else domain
        self.zone_file = os.path.join(zone_dir, name)
        self.keys_file = self.zone_file + '.keys' # File with the list of $INCLUDE
        self.signed_file = '{}.signed'.format(self.zone_file)

    def is_signed(self):
        """Check if zone is signed."""
        return os.path.isfile(self.signed_file)

    def is_up_to_date(self):
        """Check if signature is up to date."""
        return self.is_signed() and os.stat(self.zone_file).st_mtime <= os.stat(self.signed_file).st_mtime

    def check_zone_file(self):
        """Check if zone file includes key list."""
        with io.open(self.zone_file) as infh:
            for line in infh:
                if line.startswith('$INCLUDE'):
                    tokens = line.rstrip().split()
                    if len(tokens) == 2 and tokens[1] == self.keys_file:
                        return True
        logging.warning("{}: doesn't include {}".format(self.zone_file, self.keys_file))
        return False

    def content_has_changed(self, old_file, new_file):
        """Check if new file is different."""
        exists = os.path.isfile(old_file)
        logging.info('EXISTS {}: {}'.format(old_file, exists))
        if not exists:
            return True
        are_equals = filecmp.cmp(old_file, new_file)
        logging.info('EQUALS {}: {}'.format(new_file, are_equals))
        if are_equals:
            import pdb
            pdb.set_trace()
        return not are_equals
        return not os.path.isfile(old_file) or not filecmp.cmp(old_file, new_file)

    def update_zone_file(self):
        """Update links to include in zone files."""
        self.check_zone_file()
        current_keys = []
        include_stmt = '$INCLUDE '
        if os.path.isfile(self.keys_file):
            with io.open(self.keys_file) as infh:
                lineno = 0
                for line in infh:
                    lineno += 1
                    if not line.startswith(include_stmt):
                        raise SyntaxError(self.keys_file, lineno, 'expecting an include statement')
                    current_keys.append(line.rstrip()[len(include_stmt):])
        new_keys = [ key.filename for key in [ self.ksk ] + self.zsk_list ]
        if current_keys == new_keys:
            logging.debug('{}: list of keys up to date'.format(self.keys_file))
        else:
            with io.open(self.keys_file, 'w') as outfh:
                for key_name in new_keys:
                    outfh.write('$INCLUDE {}\n'.format(key_name))
            return True
        return False

    def sign(self, mode):
        if self.update_zone_file() or not self.is_up_to_date():
            ksk_name = self.ksk.name
            zsk_names = [ zsk.name for zsk in self.zsk_list ]
            logging.info('{}: signing zone with {} and {}'
                         .format(self.domain, self.ksk.display_name,
                                 ', '.join([ zsk.display_name for zsk in self.zsk_list ])))
            self.system.run(['dnssec-signzone', '-K', self.key_dir, '-o', self.domain, '-O', mode, '-t',
                             '-N', 'date', '-k', ksk_name, self.zone_file ] + zsk_names)
            return True

############################################################
# Key Management

class KeyMaster:

    """Manage the keys."""

    HRULE = '{}\n'.format("-" * 20)

    def __init__(self, domain, keys_dir, algorithm, key_size, digest_type, nsec, gid=-1, simulate=False, output=None, resolve=True):
        self.domain = domain
        if not self.domain.endswith('.'):
            self.domain += '.'
        self.keys_dir = keys_dir
        self.algorithm = algorithm
        self.key_size = key_size
        self.digest_type = digest_type
        self.nsec = nsec
        self.gid = gid
        self.capture_output = (output != None)
        self.output = output
        self.resolve = resolve
        self.system = SafeSystem(simulate)
        self.ref_time = time.time()
        self.keys = {}
        self.key_kinds = [DnsKey.KSK, DnsKey.ZSK]
        self.ds_records = None

    def load_keys(self):
        """Load key informations"""
        self.keys = {}
        for key_kind in self.key_kinds:
            self.keys[key_kind] = []
        oldcwd = os.getcwd()
        try:
            os.chdir(self.keys_dir)
            logging.debug("loading keys from {}".format(self.keys_dir))
            for filename in os.listdir('.'):
                if filename.endswith('.key'):
                    key = DnsKey.load_from_file(filename, self.ref_time)
                    logging.debug("{} {}".format(key.display_name, key.get_timeline()))
                    if key.domain == self.domain:
                        self.keys[key.kind].append(key)
        finally:
            os.chdir(oldcwd)
        for key_kind in self.key_kinds:
            self.keys[key_kind].sort(key=lambda key: key.timestamps['created'])

    def __fixperms(self):
        """Fix permissions so that the given group has read access."""
        if self.gid > -1:
            # Set read mode for group on generated files
            for filename in os.listdir(self.keys_dir):
                filepath = os.path.join(self.keys_dir, filename)
                st = os.stat(filepath)
                if st.st_gid != self.gid:
                    self.system.chown(filepath, -1, self.gid)
                mode = (stat.S_IMODE(st.st_mode) | stat.S_IRGRP) & ~stat.S_IRWXO
                if stat.S_IMODE(st.st_mode) != mode:
                    self.system.chmod(filepath, mode)

    def __dnssec(self, command, args):
        """Call a dnssec command."""
        kw = {}
        if self.capture_output:
            kw['capture_output'] = True # Not supported in python < 3.6
        result = self.system.run([command, '-K', self.keys_dir] + args, **kw)
        self.__fixperms()
        if self.capture_output and result.stdout:
            self.output.write(result.stdout.decode('UTF-8'))

    def call_keygen(self, args):
        """Generate a key."""
        self.__dnssec('dnssec-keygen', args)

    def call_dsfromkey(self, args):
        """Get DS for a key."""
        self.__dnssec('dnssec-dsfromkey', args)

    def call_settime(self, args):
        """Change key timing metadata."""
        self.__dnssec('dnssec-settime', args)

    def call_rndc(self, args, view):
        """Run rndc command."""
        if view:
            args.append('in')
            args.append(view)
        self.system.run(['rndc'] + args)

    def __rmfile(self, filename):
        """Remove a file."""
        if not os.path.isfile(filename):
            logging.warning('{}: file not found'.format(filename))
        else:
            self.system.unlink(filename)

    def notify_reload_keys(self, view=None):
        """Notify bind to reload the keys."""
        self.call_rndc(['loadkeys', self.domain], view)
        if self.nsec == 3:
            salt = '{0:08X}'.format(random.getrandbits(32))
            self.call_rndc(['signing', '-nsec3param', '1', '0', '10', salt, self.domain], view)

    def notify_reload_zone(self, view=None):
        """Notify bind to reload the zones."""
        self.call_rndc(['reload', self.domain], view)

    def is_empty(self):
        """Return true if there is no key."""
        return len(self.keys[DnsKey.KSK]) == 0

    def get_active_keys(self, key_kind):
        """Get the last activated keys of some kind."""
        keys = sorted([ key for key in self.keys[key_kind] if key.get_state() == 'activate' ])
        if not keys:
            raise Exception('no active key of kind {} found'.format(key_kind))
        return keys

    def get_keys_by_state(self, key_kind):
        """Get keys according to their current state: created, publish, activate, inactive, delete."""
        keys = {}
        for state in DnsKey.STATES:
            keys[state] = []
        for key in self.keys[key_kind]:
            keys[key.get_state()].append(key)
        return keys

    def get_signer(self, zone_dir):
        ksk_list = self.get_active_keys(DnsKey.KSK)
        zsk_list = self.get_active_keys(DnsKey.ZSK)
        return ZoneSigner(self.domain, zone_dir, self.nsec, ksk_list, zsk_list, simulate=self.system.simulate)

    def generate_key(self, key_kind, args=[]):
        """Generate a new key."""
        logging.info("generating {}".format(DnsKey.KEY_KINDS[key_kind]))
        args = [ '-a', self.algorithm.name, '-b', str(self.key_size) ] + args
        if self.nsec == 3:
            args.append('-3')
        if key_kind == DnsKey.KSK:
            args.append('-f')
            args.append('KSK')
        args.append(self.domain)
        self.call_keygen(args)

    def get_next_key(self, key_list, now, duplicates):
        """Return the key that will be activated when the current one expires.

        If there are multiple keys, return the more recent one and return the others as duplicates."""
        valid_key = None
        for key in key_list:
            activation_time = key.get_activation_time()
            logging.debug("{} {}".format(key.display_name, key.get_timeline()))
            if activation_time and activation_time > now:
                logging.info("{} in state '{}', activation date is {}".format(key.display_name, key.get_state(), format_timestamp(activation_time, ref=now)))
                if valid_key:
                    valid_key_activation_time = valid_key.get_activation_time()
                    if valid_key_activation_time > activation_time:
                        logging.warning("{} activation occurs before {}".format(key.display_name, valid_key.display_name))
                        duplicates.append(valid_key)
                        valid_key = key
                    else:
                        logging.warning("{} activation occurs after {}".format(key.display_name, valid_key.display_name))
                        duplicates.append(key)
                else:
                    valid_key = key
        return valid_key

    def get_current_key(self, key_list, now, validity_period, refresh_period, due_keys):
        """Get the current key and tell if it is expiring.

        Each key is valid over a validity period. Before the end of the period, there is a
        short time (the refresh period) where the next key must be created to be published.
            |          safe period              | refresh period |
            ^                                                    ^
        activation time                                 inactivation time
        """
        validity = ValidatePeriod.decode(validity_period)
        safe_period = validity - ValidatePeriod.decode(refresh_period)
        # if activation time is less than pivot_time the key must be renewed.
        # all keys activated before safe_period from now must be renewed.
        pivot_time = now - safe_period
        valid_key = current_key = None
        current_inactivation_time = None
        for key in key_list:
            activation_time = key.get_activation_time()
            inactivation_time = key.get_inactivation_time()
            logging.debug("{} {}".format(key.display_name, key.get_timeline()))
            if activation_time > pivot_time:
                valid_key = current_key = key
                current_inactivation_time = activation_time + validity
                logging.info("{} is valid until {}".format(key.display_name, format_timestamp(current_inactivation_time, ref=now)))
            elif inactivation_time:
                if not current_inactivation_time or inactivation_time > current_inactivation_time:
                    current_key = key
                    current_inactivation_time = inactivation_time
                logging.info('{} expires on {}'.format(key.display_name, format_timestamp(inactivation_time, ref=now)))
            else:
                logging.warning('{} rollover, {} overdue'.format(key.display_name, format_delay(pivot_time - activation_time)))
                due_keys.append(key)
                inactivation_time = activation_time + validity
                if not current_inactivation_time or inactivation_time > current_inactivation_time:
                    current_key = key
                    current_inactivation_time = inactivation_time
        return valid_key or current_key, not valid_key

    def delete_keys(self, key_list, delete_period):
        """Delete keys that have exceeded their deletion time."""
        safe_period = ValidatePeriod.decode(delete_period) / 4
        for key in key_list:
            overdue = key.get_delay_overdue('delete')
            if overdue > safe_period:
                logging.warning('{} deleting key'.format(key.display_name))
                basename = os.path.splitext(key.filename)[0]
                for extension in [ '.key', '.private' ]:
                    self.__rmfile(os.path.join(self.keys_dir, basename + extension))
            elif overdue > 0:
                logging.info('{} is unused for {}, removal in {}'.format(key.display_name, format_delay(overdue), format_delay(safe_period - overdue)))
            else:
                logging.error('{} incorrectly marked as deleted'.format(key.display_name))

    def terminate_keys(self, key_list, validity_period, refresh_period, delete_period):
        """Set inactivation time on keys after a give period."""
        for key in key_list:
            logging.info('{} setting inactivation time key'.format(key.display_name))
            self.call_settime([ '-I', '+{}'.format(refresh_period), '-D', '+{}'.format(delete_period), key.filename ])
            if self.system.is_simulate():
                validity = ValidatePeriod.decode(validity_period)
                key.set_inactivation_time(key.get_activation_time() + validity)
            else:
                key.reload()

    def get_published_ds_records(self):
        """Currently published DS records."""
        if not self.resolve:
            return None
        if self.ds_records == None:
            resolver = Resolver(self.domain)
            self.ds_records = hash_by('key_tag', resolver.get_ds_records())
        return self.ds_records

    def is_ds_record_published(self, key):
        """Check if a KSK DS record is published in parent zone."""
        ds_records = self.get_published_ds_records()
        return ds_records and int(key.id) in ds_records

    def rollover_zsk_key(self, validity_period, refresh_period, delete_period):
        """Check if a ZSK key must be renewed and create the new key if needed."""
        now = self.ref_time
        key_kind = DnsKey.ZSK
        keys = self.get_keys_by_state(key_kind)
        future_keys = keys['created'] + keys['publish']
        past_keys = keys['activate'] + keys['inactive'] + keys['delete']
        nrolls = 0
        logging.info("starting rollover at {}".format(format_timestamp(now)))
        due_keys = [] # keys that must have an inactive date
        current_key = None
        is_expiring = False
        next_key = self.get_next_key(future_keys, now, due_keys)
        if not next_key:
            # No key has been created to replace the current one
            current_key, is_expiring = self.get_current_key(past_keys, now, validity_period, refresh_period, due_keys)
        self.terminate_keys(due_keys, validity_period, refresh_period, delete_period)
        if not next_key:
            if current_key:
                if is_expiring:
                    # Create the next key. It will be published in the next hour
                    prepublication_period = int(current_key.get_inactivation_time() - now - ValidatePeriod.HOUR_IN_SECONDS)
                    logging.warning('{} renewed with prepublication period {}'.format(current_key.display_name, format_delay(prepublication_period)))
                    prepub_args = [ '-i', str(prepublication_period) ]
                    if current_key.algorithm.name != self.algorithm.name:
                        inactivation_time = current_key.get_time('inactive')
                        absolute_activation_time = format_dnssec_time(inactivation_time)
                        self.generate_key(key_kind, [ '-A', absolute_activation_time ] + prepub_args)
                    else:
                        self.call_keygen([ '-S', current_key.filename ] + prepub_args)
                    nrolls += 1
            else:
                logging.warning('no valid key and no expiring key to renew')
                keymaster.generate_key(DnsKey.ZSK)
        self.delete_keys(keys['delete'], delete_period)
        return nrolls > 0

    def rollover_ksk_key(self, validity_period, refresh_period, delete_period):
        """Check if a KSK must be activated or create a new one."""
        now = self.ref_time
        key_kind = DnsKey.KSK
        keys = self.get_keys_by_state(key_kind)
        future_keys = sorted(keys['created'] + keys['publish'])
        current_keys = keys['activate']
        due_keys = []
        current_key, is_expiring = self.get_current_key(current_keys, now, validity_period, refresh_period, due_keys)
        if not current_key:
            logging.error("no current KSK found")
        elif is_expiring:
            if future_keys:
                last_key = future_keys[-1]
                logging.info("activating {}".format(last_key.display_name))
                self.call_settime([ '-A', '+0', key.filename ])
                logging.warning("send DS record for {}".format(last_key.display_name))
                self.show_ds_record(last_key)
            else:
                self.generate_key(key_kind)
                logging.info("a new KSK has been generated run the same command again")
            return True
        elif self.is_ds_record_published(current_key):
            logging.info("{} is valid".format(current_key.display_name))
            inactivation_time = current_key.get_time('activate')
            absolute_inactivation_time = format_dnssec_time(inactivation_time)
            deletion_time = int(time.mktime(inactivation_time)) + ValidatePeriod.decode(delete_period)
            absolute_deletion_time = format_dnssec_time(time.localtime(deletion_time))
            for key in due_keys:
                logging.info('{} setting inactivation time key'.format(key.display_name))
                self.call_settime([ '-I', absolute_inactivation_time, '-D', absolute_deletion_time, key.filename ])
            self.delete_keys(keys['delete'], delete_period)
            return True
        else:
            logging.warning("send DS record for {}".format(current_key.display_name))
            self.show_ds_record(current_key)
        return False

    def show_ds_record(self, key):
        """Show a KSK DS record"""
        self.call_dsfromkey([ '-a', self.digest_type.name, key.filename ])

    def list_keys(self):
        """List the keys."""
        outfh = self.output or sys.stdout
        for key_kind in self.key_kinds:
            outfh.write("** {} **\n".format(DnsKey.KEY_KINDS[key_kind]))
            for key in self.keys[key_kind]:
                key.write_on(outfh)
                if key_kind == DnsKey.KSK:
                    self.show_ds_record(key)
                    if not self.is_ds_record_published(key):
                        logging.warning('{}: not found in {} DS records'.format(key.display_name, self.domain))
                outfh.write(self.HRULE)

def get_algorithm(name):
    """Get an algorithm by name"""
    for item in ALGORITHMS.values():
        if item.name == name:
            if not item.zone_signing:
                raise Exception("{}: algorithm not supported".format(name))
            elif item.deprecated:
                logging.warning("{}: this algorithm is weak, consider a better one".format(name))
            return item
    raise Exception("{}: unknown algorithm".format(name))

def get_digest_type(name):
    """Get a digest type by name"""
    for item in DIGEST_TYPES.values():
        if item.name == name:
            if item.deprecated:
                logging.warning("{}: this digest type is weak, consider a better one".format(name))
            return item
    raise Exception("{}: unknown digest type".format(name))

def process_domain(domain, keys_dir, gid,
                   key_type, algorithm, key_size, digest_type, nsec,
                   validity_period, refresh_period, delete_period,
                   sign_mode, zones_dir, view,
                   output, resolve=True, simulate=False):
    keymaster = KeyMaster(domain, keys_dir, algorithm, key_size, digest_type, nsec,
                          gid=gid, simulate=simulate, output=output, resolve=resolve and resolver_available)

    keymaster.load_keys()
    reload_zone = False
    if keymaster.is_empty():
        keymaster.generate_key(DnsKey.KSK)
        keymaster.generate_key(DnsKey.ZSK)
        reload_zone = True
    elif key_type:
        random.seed()
        key_kind = getattr(DnsKey, key_type)
        rollover = keymaster.rollover_zsk_key if key_kind == DnsKey.ZSK else keymaster.rollover_ksk_key
        reload_zone = rollover(validity_period, refresh_period, delete_period)
    else:
        outfh = output or sys.stdout
        outfh.write("{}\n".format('#' * 60))
        outfh.write("# Domain: {}\n".format(domain))
        keymaster.list_keys()
        if resolve and resolver_available:
            now = keymaster.ref_time
            resolver = Resolver(domain)
            try:
                resolver.resolve_ns()
            except Exception as ex:
                logging.error("{}: cannot resolve ({})".format(domain, ex))
                return
            try:
                min_expiration, max_expiration = resolver.get_expiration_time()
            except Exception as ex:
                logging.error("{}: cannot get RRSIG ({})".format(domain, ex))
                return
            if min_expiration != None or max_expiration != None:
                if min_expiration == max_expiration:
                    outfh.write("Expiration of DNS records: {}\n".format(format_timestamp(min_expiration, ref=now)))
                else:
                    outfh.write("Expiration of DNS records:\n")
                    for name, expiration in [('minimal', min_expiration), ('maximum', max_expiration)]:
                        value = '<unavailable>'
                        if expiration != None:
                            value = format_timestamp(expiration, ref=now)
                        outfh.write("  - {}: {}\n".format(name, value))
    if sign_mode:
        signer = keymaster.get_signer(zones_dir)
        if signer.sign(sign_mode):
            keymaster.notify_reload_zone(view)
    elif reload_zone:
        keymaster.notify_reload_keys(view)

def run(argv, output=None, resolve=True):
    args = parse_argv(argv)

    initialize_logger(args.verbose, args.debug, args.use_syslog)

    locale.setlocale(locale.LC_ALL, '')
    gid = get_group_id(args.group)

    algorithm = get_algorithm(args.algorithm)
    digest_type = get_digest_type(args.digest_type)
    for domain in args.domains:
        process_domain(domain, args.keys_dir, gid,
                       args.key_type, algorithm, args.key_size, digest_type, args.nsec,
                       args.validity_period, args.refresh_period, args.delete_period,
                       args.sign_mode, args.zones_dir, args.view,
                       output, resolve=resolve, simulate=args.simulate)

def main(argv=None):
    try:
        run(argv)
    except SystemExit:
        raise
    except:
        import traceback
        traceback.print_exc()
        sys.exit(1)

if __name__ == '__main__':
    main()

#  Local Variables: ***
#  mode: python ***
#  End: ***
