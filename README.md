KeyRoller
=========

[![build status](https://gitlab.com/lp-sysadmin-tools/keyroller/badges/master/pipeline.svg)](https://gitlab.com/lp-sysadmin-tools/keyroller/commits/master)
[![license](https://img.shields.io/badge/license-GPL3-lightgrey.svg)](https://gitlab.com/lp-sysadmin-tools/keyroller/raw/master/COPYING)

Manage DNSSec keys rollover for Bind 9 in auto signing mode. Starting from version 2.0, manual signing is supported.

The command must be run once a day, for example with cron. It doesn't run as a daemon.

Installation
------------

The command uses *dnssec-keygen*, *dnssec-settime* and *rndc*. In Debian, they are in the
package *bind9utils*. In Archlinux, they are in *bind-tools*.

It consists in a single script file keyroller.py.

Alternatively, it can be installed with pip from the source folder:

    $ pip install .

Usage
-----

When executed the first time, the KSK and ZSK keys are generated.

In the following example, the ZSK keys are valid for 30 days. They are renewed in the last
5 days of their validity period. They are deleted one month after they have expired.

    keyroller --key-type=ZSK --validity=30d --refresh=5d --delete=1mo example.org /etc/bind/keys

If there are multiple views in the configuration, use option `--view`. For example: `--view=external`

If user that runs *bind* is not root, gives the group it belongs with option `--group`. For example,
on Debian, use `--group=bind`.

The script is supposed to be run automatically every days for ZSK for example from a script in /etc/cron.daily.

Manual signing
--------------

It's available with option `--sign=FORMAT` where _FORMAT_ is the output format supported
by dnssec-signzone. For example for `raw`, the zone definition must include:

    masterfile-format raw;

The zone must include the file generated for with the keys records:

    $INCLUDE /etc/bind/zone/DOMAIN.keys

ZSK Renewal
-----------

A new ZSK is created in the “refresh period” before the end of validity of the current ZSK.

    <-------------------- Validity period -------------------->
    <----------- Safe period -----------><-- Refresh period -->

If the script is called in the “refresh period”, a new ZSK is created. It will be activated
at the end of the current ZSK “validity period”.

KSK Renewal
-----------

The same mechanism applies for the KSK but it's a manual process.

    keyroller --key-type=KSK --validity=3y --refresh=1mo --delete=3mo example.org /etc/bind/keys

Use options `--group` and `--view` as for the ZSK renewal if necessary.

If the validity of the key has expired, a new key is generated.

    WARNING: KSK 44407 rollover, 658 days overdue
    Generating key pair.
    Kexample.org.+013+31487

Each time the same command is run, it checks if the DS record in the parent domain is up to date.

    WARNING: send DS record for KSK 31487
    example.org. IN DS 31487 13 2 F0B4EC68D4D6328D5056258B7A808D1B84C830104D1B790DD6A155ED17C624E1

Once the record is available, the old key is deactivated and later deleted.
