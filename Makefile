COVERAGE_RUN = env PYTHONPATH=$(PWD) coverage run --source=keyroller
COVERAGE_REPORT = coverage report -m

build-dist:
	python setup.py bdist

test:
	env PYTHONPATH=$(PWD) python -m unittest discover

test-%:
	env PYTHONPATH=$(PWD) python tests/test_$*.py

debug-%:
	env PYTHONPATH=$(PWD) python -m pdb tests/test_$*.py

coverage:
	$(COVERAGE_RUN) -m unittest discover
	$(COVERAGE_REPORT)

clean:
	-rm -rf *.egg-info build dist
	-rm -rf __pycache__

release:
	git diff --quiet || { echo "Error: there are uncommited changes"; exit 1; }
	VERSION=$$(sed -n "/^version/s/^version = '\\(.*\\)'/\\1/p" keyroller.py) &&	\
		git tag -a -m "Release $$VERSION" r$$VERSION &&				\
		git push --follow-tags
